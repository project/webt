# WEB-T Drupal module 
Automated website translation solution for Drupal websites. 
This module can pre-translate all entities (articles, pages, comments, tags), UI and config strings and translate new entities when they are created or updated. Before loading any translatable entity, this module also translates them, if translation in selected language does not exist. It does that by adding/updating entity, UI and config string translations provided by Drupal core localization modules. 

## Dependencies
Module depends on Drupal built-in localization modules:
- language - provides website language selection functionality;
- content_translation - provides API for Drupal entity (page, article, etc.) translation;
- config_translation - provides API for configuration item translation;
- locale - provides API for user interface item translation.

For machine-translation this module uses either eTranslation or any other MT API integrator compatible MT provider. Machine translation configuration and access key is specified by website administrator in module settings.

## Architecture

![Module architecture](webt-architecture.png)

## Setup
1. Copy repository contents to a new folder called `webt`, then copy this folder to Drupal website's `modules/custom` folder or create ZIP archive of it and install ZIP via `Extend->Add new module` admin page.
2. Clear cache by using `Configuration->Performance->Clear all caches` button or by running `drush cr` command.
3. Go to `Extend->List` section and under `Multilingual` section select 'WEB-T' module, click `Install`. If asked, confirm installation of module dependencies.
4. Go to `Configuration->Languages` and add translation languages.
5. Go to `Configuration->Content language and translation` and enable content translation for all website content elements you want to translate automatically (e.g. Comment, Content, Taxonomy term) by marking them as 'Translatable' and saving settings. 
6. Go to `Configuration->WEB-T settings` and choose MT engine. For eTranslation enter eTranslation API credentials; for Custom provider - specify provider's Base URL and API key. Click `Save`.
7. Translate all exisiting and translatable elements by selecting target languages and clicking `Translate` in Machine translation section.

## Screenshots

Module settings:


![Settings](webt.png)

## License
This module is licensed under the GNU General Public License, version 2 (GPLv2) or later

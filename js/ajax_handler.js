(function ($, Drupal) {
    'use strict';

  var cancelled = false;
  var ajaxError = false;
  var success = false;

  // cancel pre-translation process on page leave
  $(window).on('beforeunload', function () {
    if ($.active && !ajaxError && !success) {
      cancelled = true;
      $.get(Drupal.url('webt_pretranslation_cancel'), function (data, textStatus, jqXHR) {});
    }
  });

  // add error notification and reload the page on ajax error
  $(document).ajaxError(function (event, xhr, settings, thrownError) {
    if (settings.extraData && settings.extraData._triggering_element_name && !cancelled && !success) {
      ajaxError = true;
      $.get(Drupal.url('webt_pretranslation_set_ajax_error'), function (data, textStatus, jqXHR) {
        location.reload();
      });
    }
  });

  // reload page on translation completion to show success notifications & update progress table.
  $(document).ajaxComplete(function (event, xhr, settings) {
    if (settings.extraData && settings.extraData._drupal_ajax && settings.extraData._triggering_element_value ) {
      success = true;
      location.reload();
    }
  });

})(jQuery, Drupal);

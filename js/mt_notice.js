function hideMtNotice() {
    jQuery(".mt-notice-container").hide();
    jQuery(".mt-notice-space").hide();
}

jQuery( document ).ready(function () {
    var text = drupalSettings.mt_notice_settings.text;
    var linkText = drupalSettings.mt_notice_settings.linkText
    var originalUrl = drupalSettings.path.baseUrl + drupalSettings.path.currentPath;

    jQuery('body').prepend(
        `<div class="mt-notice-container">\
            <div class="translation-notice">\
                ${text} <a href="${originalUrl}" class="mt-notice-link">${linkText}</a>\
            </div>\
            <div id="mt-notice-hide" onclick="hideMtNotice()">&times;</div>\
        </div>\
        <div class="mt-notice-space"></div>`
    );

    var mtNoticeContainer = jQuery(".mt-notice-container");
    if(mtNoticeContainer.length){
        jQuery(".mt-notice-space").css("height", mtNoticeContainer.height());
    }
});

<?php

namespace Drupal\webt\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\webt\WebtConstInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller that handles ajax pre-translation progress requests from frontend.
 */
class PretranslationProgressController extends ControllerBase {

  /**
   * Cache key that stores pre-translation progress value.
   *
   * @var string
   */
  public static $progressKey = 'webt_translation_progress';

  /**
   * Logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * URL generator.
   *
   * @var Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * Constructs a new class instance.
   *
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $urlGenerator
   *   URL generator.
   */
  public function __construct(UrlGeneratorInterface $urlGenerator) {
    $this->urlGenerator = $urlGenerator;
    $this->logger = $this->getLogger(WebtConstInterface::WEBT_LOGGER);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('url_generator')
    );
  }

  /**
   * Retrieves translation progress message and percentage from Drupal cache.
   *
   * @return Symfony\Component\HttpFoundation\Response
   *   JSON progress response
   */
  public function getTranslationProgress() {
    $response = NULL;
    $value    = $this->cache()->get(self::$progressKey);
    if ($value && $value->data) {
      $response = $value->data;
    }
    else {
      $empty_response             = new \stdClass();
      $empty_response->message    = '';
      $empty_response->percentage = 0;
      $response                   = $empty_response;
    }
    return new Response(json_encode($response));
  }

  /**
   * Removes translation progress message and percentage from Drupal cache.
   *
   * @return Symfony\Component\HttpFoundation\Response
   *   Empty response
   */
  public function cancelTranslation() {
    $this->logger->error('Client page was closed, cancelling pre-translation...');
    $this->updateProgress(NULL, NULL, TRUE);
    return new Response();
  }

  /**
   * Add error notification (to be called on ajax error)
   *
   * @return Symfony\Component\HttpFoundation\Response
   *   Empty response
   */
  public function ajaxErrorHandler() {
    $this->messenger()->addError($this->t('Backend translation process was aborted! Check <a href=":url">logs</a> for errors or try increasing website\'s PHP max_execution_time.', [':url' => $this->urlGenerator->generate('dblog.overview')]));
    return new Response();
  }

  /**
   * Update progress bar message.
   *
   * @param string $message
   *   Message to be shown below progress bar.
   */
  public function updateTranslationMessage($message) {
    $this->updateProgress(NULL, $message);
  }

  /**
   * Update progress bar percentage.
   *
   * @param float $percentage
   *   Percentage value to be shown in progress bar.
   */
  public function updateTranslationPercentage($percentage) {
    $this->updateProgress($percentage);
  }

  /**
   * Delete translation progress information from cache.
   */
  public function clearTranslationProgress() {
    $this->cache()->delete(self::$progressKey);
  }

  /**
   * Retrieve translation progress information from cache.
   *
   * @return stdClass
   *   Progress data
   */
  public function getValue() {
    return $this->cache()->get(self::$progressKey);
  }

  /**
   * Update translation progress information in cache.
   *
   * @param float $percentage
   *   New percentage.
   * @param string $message
   *   New message.
   * @param bool $aborted
   *   Is this translation process aborted.
   */
  private function updateProgress($percentage = NULL, $message = NULL, $aborted = FALSE) {
    $value  = $this->cache()->get(self::$progressKey);
    $update = NULL;
    if ($value && $value->data) {
      $update = $value->data;
    }
    else {
      $update             = new \stdClass();
      $update->message    = '';
      $update->percentage = 0;
      $update->aborted    = FALSE;
      $update->request_id = uniqid();
    }
    if ($percentage !== NULL) {
      $update->percentage = round($percentage);
    }
    if ($message) {
      $update->message = $message;
    }
    if ($aborted) {
      $update->aborted = $aborted;
    }
    $this->cache()->set(self::$progressKey, $update);
  }

}

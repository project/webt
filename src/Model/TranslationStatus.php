<?php

namespace Drupal\webt\Model;

/**
 * Multi-request translation status.
 */
class TranslationStatus {
  // Translation failed.
  const NO_RESULT = -1;
  // Some translation requests were successful,
  // but translation could not be finished.
  const PARTIAL_RESULT = 0;
  // All translation requests were succcessful.
  const FULL_RESULT = 1;

}

<?php

namespace Drupal\webt;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Url;

/**
 * Service that generates public URL for assets (e.g. images).
 */
class AssetManager {

  /**
   * Module handler.
   *
   * @var Drupal\Core\Extension\ModuleHandlerInterface
   */
  private $moduleHandler;

  /**
   * Language manager.
   *
   * @var Drupal\Core\Language\LanguageManagerInterface
   */
  private $languageManager;

  /**
   * Constructor.
   *
   * @param Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   Module handler.
   * @param Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   Language manager.
   */
  public function __construct(ModuleHandlerInterface $module_handler, LanguageManagerInterface $language_manager) {
    $this->moduleHandler   = $module_handler;
    $this->languageManager = $language_manager;
  }

  /**
   * Retrieves full asset URL from relative path.
   *
   * @param string $relative_path
   *   Path to asset.
   *
   * @return string
   *   Asset URL
   */
  public function getAssetUrl($relative_path) {
    $module_path = $this->moduleHandler
      ->getModule('webt')
      ->getPath();

    $current_language = $this->languageManager->getCurrentLanguage()->getId();
    $home_url         = Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString();
    // Remove language code from URL if present.
    if (str_ends_with($home_url, '/' . $current_language)) {
      $home_url = substr($home_url, 0, strlen($home_url) - strlen($current_language));
    }

    return "$home_url/$module_path/$relative_path";
  }

}

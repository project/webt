<?php

namespace Drupal\webt\Form;

use Drupal\content_translation\ContentTranslationManager;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\Url;
use Drupal\webt\Controller\PretranslationProgressController;
use Drupal\webt\Model\ProcessingResponse;
use Drupal\webt\Model\StringType;
use Drupal\webt\TranslationManager;
use Drupal\webt\WebtConstInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Pre-translation tab form.
 */
class PretranslationForm extends ConfigFormBase {

  /**
   * Translation manager service.
   *
   * @var \Drupal\webt\TranslationManager
   */
  protected $translationManager;

  /**
   * Content translation manager service.
   *
   * @var \Drupal\content_translation\ContentTranslationManager
   */
  protected $contentTranlationManager;

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Pre-translation progress controller.
   *
   * @var \Drupal\webt\Controller\PretranslationProgressController
   */
  protected $pretranslationProgressController;

  /**
   * URL generator.
   *
   * @var Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * Logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new class instance.
   *
   * @param \Drupal\Core\StringTranslation\TranslationManager $translation_manager
   *   The translation manager service.
   * @param \Drupal\Core\Entity\ContentTranslationManagerInterface $content_tranlation_manager
   *   The content translation manager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   * @param \Drupal\webt\Controller\PretranslationProgressController $pretranslation_progress_controller
   *   Pre-translation progress controller.
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $urlGenerator
   *   URL generator.
   */
  public function __construct(
      TranslationManager $translation_manager,
      ContentTranslationManager $content_tranlation_manager,
      EntityTypeManagerInterface $entity_type_manager,
      LanguageManagerInterface $language_manager,
      PretranslationProgressController $pretranslation_progress_controller,
      UrlGeneratorInterface $urlGenerator
    ) {
    $this->logger                           = $this->getLogger(WebtConstInterface::WEBT_LOGGER);
    $this->translationManager               = $translation_manager;
    $this->contentTranlationManager         = $content_tranlation_manager;
    $this->entityTypeManager                = $entity_type_manager;
    $this->languageManager                  = $language_manager;
    $this->pretranslationProgressController = $pretranslation_progress_controller;
    $this->urlGenerator                     = $urlGenerator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('webt.translation_manager'),
      $container->get('content_translation.manager'),
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
      $container->get('webt.pretranslation_progress_controller'),
      $container->get('url_generator'),
    );
  }

  /**
   * Drupal form id.
   *
   * @return string
   *   Form ID
   */
  public function getFormId() {
    return 'PretranslationForm';
  }

  /**
   * Adds fields and libraries to the form.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return Drupal\Core\Form\FormInterface
   *   Form definition
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $selected_types = array_filter(
      StringType::getAllTypes(),
      function ($type) {
        return $this->isStringTypeSelected($type);
      },
    );
    $selected_types = array_map(
      function ($type) {
        return StringType::getDisplayName($type);
      },
      $selected_types
    );

    $form['description'] = [
      '#type'   => 'markup',
      '#markup' => '<div class="form-item__description">' . $this->t('Follow translation progress and translate existing content. New content will be translated automatically.') . '</div>',
    ];

    $noteTitle    = $this->t('Note');
    $noteText     = $this->t('Translating different languages or content types separately can be more effiecient.');
    $form['note'] = [
      '#type'   => 'markup',
      '#markup' => '<div class="form-item__description"><strong>' . $noteTitle . ': </strong>' . $noteText . '</div>',
    ];

    $form['table'] = $this->getTranslationProgressTable();

    $translate_button = [
      '#type'  => 'submit',
      '#value' => $this->t('Translate'),
      '#ajax'  => [
        'callback' => '::translateItems',
        'event'    => 'click',
        'progress' => [
          'type'     => 'bar',
          'message'  => 'Translating...',
          'url'      => Url::fromRoute('webt.pretranslation.progress'),
          'interval' => '500',
        ],
      ],
    ];

    $confirm_deletion = $this->t('Are you sure you want to delete translations of selected languages?');
    $delete_button = [
      '#type'       => 'submit',
      '#value'      => $this->t('Delete translations'),
      '#submit'     => ['::deleteItems'],
      '#attributes' => [
        'title'   => $this->t('Delete translations of selected languages and types'),
        'onclick' => "return confirm('$confirm_deletion');",
      ],
    ];

    $form['button_wrapper'] = [
      '#type'            => 'container',
      '#attributes'      => ['class' => ['text-align-right']],
      'delete'           => $delete_button,
      'translate_button' => $translate_button,
    ];

    $form['advanced'] = $this->getAdvancedOptions();

    $form['#attached']['library'][]                        = 'core/drupal.ajax';
    $form['#attached']['library'][]                        = 'webt/pretranslation_tab.style';
    $form['#attached']['library'][]                        = 'webt/pretranslation_tab.js';
    $form['#attached']['drupalSettings']['selected_types'] = $selected_types;

    return $form;
  }

  /**
   * Creates Advanced option section with fields.
   *
   * @return array
   *   Advanced option section definition
   */
  private function getAdvancedOptions() {
    $advanced_options = [
      '#type'  => 'details',
      '#title' => $this->t('Content types'),
      '#open'  => FALSE,
      '#tree'  => TRUE,
    ];

    $advanced_options['description'] = [
      '#type'   => 'markup',
      '#markup' => '<div class="form-item__description">' . $this->t('Selected content types to be translated/deleted.') . '</div>',
    ];

    $advanced_options[StringType::NODE] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Nodes'),
      '#default_value' => $this->isStringTypeSelected(StringType::NODE),
      '#description'   => $this->t('Translate untranslated node type elements (articles, pages)'),
    ];

    $advanced_options[StringType::COMMENT] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Comments'),
      '#default_value' => $this->isStringTypeSelected(StringType::COMMENT),
      '#description'   => $this->t('Translate untranslated comments'),
    ];

    $advanced_options[StringType::TAG] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Tags'),
      '#default_value' => $this->isStringTypeSelected(StringType::TAG),
      '#description'   => $this->t('Translate untranslated taxonomy terms (tags)'),
    ];

    $advanced_options[StringType::CONFIG] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Configuration'),
      '#default_value' => $this->isStringTypeSelected(StringType::CONFIG),
      '#description'   => $this->t('Translate untranslated configuration strings (site name, role names, blocks, etc.)'),
    ];

    $advanced_options[StringType::UI] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('UI elements'),
      '#default_value' => $this->isStringTypeSelected(StringType::UI),
      '#description'   => $this->t('Translate untranslated UI elements'),
    ];

    $advanced_options['save_wrapper'] = [
      '#type'       => 'container',
      '#attributes' => ['class' => ['text-align-right']],
      'save'        => [
        '#type'       => 'submit',
        '#value'      => $this->t('Save'),
        '#attributes' => ['title' => $this->t('Save affected content type selection')],
      ],
    ];

    // Checks if specific content types are not translatable
    // due to website configuration.
    $this->checkContentTypeTranslationEnabled(StringType::NODE, [
      'article',
      'page',
    ]);
    $this->checkContentTypeTranslationEnabled(StringType::COMMENT, ['comment']);
    $this->checkContentTypeTranslationEnabled(StringType::TAG, ['tags']);

    return $advanced_options;
  }

  /**
   * Updates content type selection on Advanced option Save button click.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('webt.settings');
    $types  = StringType::getAllTypes();
    foreach ($types as $type) {
      $selected = $form_state->getValue('advanced')[$type];
      $config->set('translate_' . $type, $selected);
    }
    $config->save();
    $this->messenger()->addStatus($this->t('Affected content type selection saved'));
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'webt.settings',
    ];
  }

  /**
   * Checks if translation is enabled in site settings for given content type.
   *
   * If content type is marked as translatable in this
   * module but not in website config, warning is added.
   *
   * @param string $entity_type_id
   *   Content type ID.
   * @param string[] $bundle_names
   *   Array of properties to check for given $entity_type_id.
   *
   * @return bool
   *   True, if content type tranlation is enabled, false otherwise
   */
  public function checkContentTypeTranslationEnabled($entity_type_id, $bundle_names) {
    foreach ($bundle_names as $bundle_name) {
      if ($this->contentTranlationManager->isEnabled($entity_type_id, $bundle_name)) {
        return TRUE;
      }
    }
    if ($this->isStringTypeSelected($entity_type_id)) {
      $this->messenger()->addWarning(
        $this->t(
          '@strings_of_type are not set as translatable in <a href=":url">settings</a>!',
          [
            '@strings_of_type' => StringType::getDisplayName($entity_type_id),
            ':url' => $this->urlGenerator->generate('language.content_settings_page'),
          ]
      ));

    }

    return FALSE;
  }

  /**
   * Pre-translate website to selected languages and content types.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return Drupal\Core\Ajax\AjaxResponse
   *   Response
   */
  public function translateItems(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $this->messenger()->deleteAll();

    if (!$this->validatePretranslationRequest()) {
      return $response;
    }

    $this->pretranslationProgressController->clearTranslationProgress();
    $this->pretranslationProgressController->updateTranslationMessage($this->t('Translation started'));
    $this->pretranslationProgressController->updateTranslationPercentage(0);

    $selected_langcodes = array_filter($form_state->getValue('table'));
    if (empty($selected_langcodes)) {
      $this->messenger()->addWarning($this->t('Please select at least one target language'));
      return $response;
    }
    $selected_langcodes = $this->filterSupportedLanguages($selected_langcodes);

    if ($this->isStringTypeSelected(StringType::TAG)) {
      $this->translateEntities(StringType::TAG, $selected_langcodes);
    }
    if ($this->isStringTypeSelected(StringType::NODE)) {
      $this->translateEntities(StringType::NODE, $selected_langcodes);
    }
    if ($this->isStringTypeSelected(StringType::COMMENT)) {
      $this->translateEntities(StringType::COMMENT, $selected_langcodes);
    }
    if ($this->isStringTypeSelected(StringType::CONFIG)) {
      $this->translateConfigurationStrings($selected_langcodes);
    }
    if ($this->isStringTypeSelected(StringType::UI)) {
      $this->translateUserInterfaceElements($selected_langcodes);
    }
    drupal_flush_all_caches();

    $this->logger->info('Website translation finished');
    return $response;
  }

  /**
   * Translate all entities of give type.
   *
   * @param string $entity_type
   *   Entity type.
   * @param string[] $langcodes
   *   Target language codes.
   */
  public function translateEntities($entity_type, $langcodes) {
    $this->logger->debug("Translating entities of type: '@entity_type'", ['@entity_type' => $entity_type]);

    $entities = $this->entityTypeManager
      ->getStorage($entity_type)
      ->loadMultiple();

    $response = $this->translationManager->translateEntities($entities, $entity_type, $langcodes);
    $this->registerTranslationResults($response, $entity_type);
  }

  /**
   * Translate UI strings.
   *
   * @param string[] $langcodes
   *   Target language codes.
   */
  public function translateUserInterfaceElements($langcodes) {
    $this->logger->debug('Translating UI elements');
    $source_language = $this->translationManager->getDefaultLanguageCode();

    $results = [ProcessingResponse::$alreadyProcessed];
    foreach ($langcodes as $langcode) {
      if ($langcode === $source_language) {
        continue;
      }
      $untranslated_strings = $this->translationManager->getUntranslatedUiStrings($langcode);
      $results[]            = $this->translationManager->translateUiStrings($untranslated_strings, $source_language, $langcode);
    }
    $response = min($results);
    $this->registerTranslationResults($response, StringType::UI);
  }

  /**
   * Translate configuration strings.
   *
   * @param string[] $langcodes
   *   Target language codes.
   */
  public function translateConfigurationStrings($langcodes) {
    $this->logger->debug('Translating configurations strings');
    $this->pretranslationProgressController->updateTranslationPercentage(0);

    $source_language = $this->translationManager->getDefaultLanguageCode();

    $results = [ProcessingResponse::$alreadyProcessed];
    foreach ($langcodes as $langcode) {
      if ($langcode === $source_language) {
        continue;
      }
      $strings   = $this->translationManager->getUntranslatedConfigStrings($langcode);
      $results[] = $this->translationManager->translateConfigStrings($strings, $langcode);
    }
    $response = min($results);
    $this->registerTranslationResults($response, StringType::CONFIG);
  }

  /**
   * Delete translations for selected languages and content types.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function deleteItems(array &$form, FormStateInterface $form_state) {
    $selected_langcodes = array_filter($form_state->getValue('table'));
    if (empty($selected_langcodes)) {
      $this->messenger()->addWarning($this->t('Please select at least one target language'));
      return;
    }

    if ($this->isStringTypeSelected(StringType::TAG)) {
      $this->deleteEntityTranslations(StringType::TAG, $selected_langcodes);
    }
    if ($this->isStringTypeSelected(StringType::NODE)) {
      $this->deleteEntityTranslations(StringType::NODE, $selected_langcodes);
    }
    if ($this->isStringTypeSelected(StringType::COMMENT)) {
      $this->deleteEntityTranslations(StringType::COMMENT, $selected_langcodes);
    }
    if ($this->isStringTypeSelected(StringType::CONFIG)) {
      $this->logger->debug('Deleting configuration translations');
      $response = $this->translationManager->deleteConfigTranslations($selected_langcodes);
      $this->registerDeletionResults($response, StringType::CONFIG);
    }
    if ($this->isStringTypeSelected(StringType::UI)) {
      $this->logger->debug('Deleting UI string translations');
      $response = $this->translationManager->deleteUiTranslations($selected_langcodes);
      $this->registerDeletionResults($response, StringType::UI);
    }
  }

  /**
   * Delete entity translations for given languages.
   *
   * @param string $entity_type
   *   Entity type ID.
   * @param string[] $langcodes
   *   Target language codes.
   */
  public function deleteEntityTranslations($entity_type, $langcodes) {
    $this->logger->debug("Deleting entity translations of type: '@entity_type'", ['@entity_type' => $entity_type]);

    $entities = $this->entityTypeManager
      ->getStorage($entity_type)
      ->loadMultiple();

    $response = $this->translationManager->deleteTranslationsOfEntities($entities, $langcodes);
    $this->registerDeletionResults($response, $entity_type);
  }

  /**
   * Build translation progress table.
   *
   * @return array
   *   Translation progress table definition
   */
  public function getTranslationProgressTable() {
    $types                = StringType::getAllTypes();
    $langcodes            = $this->languageManager->getLanguages();
    $langcodes_list       = array_keys($langcodes);
    $translation_progress = [];

    foreach ($langcodes_list as $langcode) {
      $translation_progress[$langcode] = [];
      foreach ($types as $type) {
        $translation_progress[$langcode][$type] = $this->translationManager->getTranslationProgressPercents($type, $langcode);
      }
    }

    $rows = [];
    foreach ($translation_progress as $langcode => $value) {
      $language     = $langcodes[$langcode]->getName();
      $row          = [$language];
      $row['title'] = [
        'data' => [
          '#title' => $language,
        ],
      ];
      foreach ($value as $progress) {
        $row[] = round($progress, 2) . '%';
      }
      $rows[$langcode] = $row;
    }

    $display_types = array_map(
    function ($key) {
      return StringType::getDisplayName($key);
    },
            $types
    );

    $header = [$this->t('Language')] + $display_types;
    return [
      '#type'    => 'tableselect',
      '#header'  => array_values($header),
      '#options' => $rows,
    ];
  }

  /**
   * Add translation finished notification based on given status response.
   *
   * @param int $response
   *   Pre-translation process response.
   * @param string $display_type
   *   Type of strings that were translated.
   */
  public function registerTranslationResults($response, $display_type) {
    switch ($response) {
      case ProcessingResponse::$fullyProcessed:
        $this->messenger()->addStatus($this->t(
          "Type '@display_type' strings translated successfully!",
          ['@display_type' => $display_type]
        ));
        return;

      case ProcessingResponse::$partiallyProcessed:
        $this->messenger()->addWarning($this->t(
          'Type "@display_type" strings translated partially. Try decreasing MT engine request size (Settings->Advanced->Max characters per single request) or check <a href=":url">logs</a> for errors.',
          ['@display_type' => $display_type, ':url' => $this->urlGenerator->generate('dblog.overview')]
        ));
        return;

      case ProcessingResponse::$processingError:
        $this->messenger()->addError($this->t(
          'Type "@display_type" strings were not translated. Check your machine-translation configuration or <a href=":url">logs</a>.',
          ['@display_type' => $display_type, ':url' => $this->urlGenerator->generate('dblog.overview')]
        ));
        return;

      case ProcessingResponse::$alreadyProcessed:
        return;

      default:
        $this->logger->error("Invalid translation response '@response'. Please contact the developers of this module!", ['@response' => $response]);
        return;
    }
  }

  /**
   * Add translations deleted notification based on given status response.
   *
   * @param int $response
   *   Deletion process response.
   * @param string $display_type
   *   Type of translations that were deleted.
   */
  public function registerDeletionResults($response, $display_type) {
    switch ($response) {
      case ProcessingResponse::$fullyProcessed:
        $this->messenger()->addStatus($this->t(
          "Type '@display_type' strings deleted successfully!",
          ['@display_type' => $display_type]
        ));
        return;

      default:
        $this->logger->error("Invalid deletion response '@response'. Please contact the developers of this module!", ['@response' => $response]);
        return;
    }
  }

  /**
   * Checks if translation is enabled in module config for given string type.
   *
   * @param string $type
   *   Translatable object type.
   *
   * @return bool
   *   True if string type is selected, false otherwise.
   */
  private function isStringTypeSelected($type) {
    $config = $this->config('webt.settings');
    $value = $config->get('translate_' . $type);
    return $value === NULL || $value;
  }

  /**
   * Filters given langcode list & returns langcodes supported by MT provider.
   *
   * @param string[] $selected_langcodes
   *   Target language codes.
   *
   * @return array
   *   Supported language code list
   */
  private function filterSupportedLanguages($selected_langcodes) {
    $supported_languages = [];
    $translation_service = $this->translationManager->translationService;
    foreach ($selected_langcodes as $langcode) {
      $language = $this->languageManager->getLanguageName($langcode);
      if (!$translation_service->isLanguageSupported($langcode)) {
        $this->messenger()->addWarning($this->t('Language is not supported by MT provider: @language', ['@language' => $language]));
      }
      else {
        $supported_languages[] = $langcode;
      }
    }
    return $supported_languages;
  }

  /**
   * Check if pre-translation request is valid.
   *
   * @return bool
   *   True if pre-translation request is still valid, false otherwise
   */
  private function validatePretranslationRequest() {
    $translation_service = $this->translationManager->translationService;
    if (!$translation_service->isAuthorized()) {
      $this->messenger()->addError($this->t('Machine translation provider authorization data is not configured. Please go to Translation provider section and enter your MT provider data!'));
      return FALSE;
    }
    return TRUE;
  }

}

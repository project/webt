<?php

namespace Drupal\webt\translation_engines;

use Drupal\Component\Utility\Html;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\webt\Model\TranslationResponse;
use Drupal\webt\Model\TranslationStatus;
use Drupal\webt\WebtConstInterface;

/**
 * Translation service base class.
 */
abstract class AbstractTranslationService {

  use LoggerChannelTrait;
  use StringTranslationTrait;

  /**
   * Regex patterns for non-translatable Drupal variables in UI strings.
   *
   * @var array
   */
  private $drupalVariablePatterns = [
        // Match @.. and %.. and !..
    '(([^(@|%|!)^\sa-zA-Z\d+<>]*)(@|%|!)([^\s<>(@|%|!)]+))',
        // Match curly bracket variables like {var}.
    '({([^\s]*?)})',
        // Match double curly bracket variables like {{ var }}.
    '({{\s*(.*?)\s*}})',
        // Match square bracket variables like [site:name].
    '(\[(.*?)\:(.*?)\])',
        // Match PHP variables like $var['abc'].
    '(\$(([a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*(->)*)*(\[[^\]]*\])*))',
  ];

  /**
   * HTML wrapper prefix for every translatable string.
   *
   * @var string
   */
  private $htmlWrapperPrefix = '<div>';
  /**
   * HTML wrapper suffix for every translatable string.
   *
   * @var string
   */
  private $htmlWrapperSuffix = '</div>';
  /**
   * Char dictionary for chars to replace before string is converted to HTML.
   *
   * @var array
   */
  private $specialCharDict = ["\x03" => '<etx></etx>'];
  /**
   * List of translatable HTML attributes.
   *
   * @var array
   */
  private $translatableHtmlAttributes = ['alt'];

  /**
   * HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;
  /**
   * Whether to retry entity translation on error.
   *
   * @var bool
   */
  protected $retryOnEntityTranslationError = TRUE;
  /**
   * Char limit when to split translatable strings into separate request.
   *
   * @var int
   */
  protected $maxCharsPerRequest = 60000;
  /**
   * Drupal configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Initializes service.
   *
   * @param \Drupal\Core\Http\ClientFactory $http_client_factory
   *   HTTP client factory.
   */
  public function __construct(ClientFactory $http_client_factory) {
    $this->httpClient = $http_client_factory->fromOptions(['timeout' => 120]);
    $this->config     = \Drupal::config('webt.settings');
    $this->logger     = $this->getLogger(WebtConstInterface::WEBT_LOGGER);
  }

  /**
   * Translation request function implemented by each MT provider.
   *
   * @param string $from
   *   Source langcode.
   * @param string $to
   *   Target langcode.
   * @param string[] $values
   *   Translatable XML strings.
   * @param Drupal\Core\Entity\EntityInterface $entity
   *   Related entity.
   *
   * @return string[]
   *   Translations
   */
  abstract protected function sendTranslationRequest($from, $to, $values, $entity);

  /**
   * Language direction request function implemented by each MT provider.
   *
   * @return Psr\Http\Message\ResponseInterface
   *   Response
   */
  abstract protected function sendLanguageDirectionRequest();

  /**
   * Checks if MT authorization data is provided.
   *
   * @return bool
   *   True if MT provider data is configured, false otherwise
   */
  abstract public function isAuthorized();

  /**
   * Gets languages supported by MT API from cache or by making an HTTP request.
   *
   * @return array
   *   MT provider supported languages
   */
  public function getSupportedLanguages() {
    $key         = 'language_directions';
    $config      = \Drupal::getContainer()->get('config.factory')->getEditable('webt.settings');
    $json_string = $config->get($key);
    if (!$json_string) {
      $response = $this->sendLanguageDirectionRequest();
      if ($response && 200 === $response->getStatusCode()) {
        $json_string = $this->mapLanguageDirectionResponseBody($response->getBody());
        $config->set($key, $json_string);
        $config->save();
      }
      else {
        $result = [];
        $config->set($key, $result);
        $config->save();
        return $result;
      }
    }
    $obj = json_decode($json_string);
    return $obj->languageDirections ?? [];
  }

  /**
   * Checks if given langcode is supported by MT provider.
   *
   * @param string $langcode
   *   Language code.
   *
   * @return bool
   *   True, if language is supported, false otherwise
   */
  public function isLanguageSupported($langcode) {
    $lang                = substr($langcode, 0, 2);
    $supported_languages = $this->getSupportedLanguages();
    foreach ($supported_languages as $engine) {
      if ($engine->trgLang === $lang) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Translates given string values.
   *
   * @param string $from
   *   Source langcode.
   * @param string $to
   *   Target langcode.
   * @param string[] $values
   *   String array of translatable values.
   * @param string $object_type
   *   String content type related to given translatable values.
   * @param bool $encodePlaceholders
   *   Whether to encode non-translatable Drupal variables before translation.
   * @param Drupal\Core\Entity\EntityInterface $entity
   *   Related entity.
   *
   * @return string[]
   *   Translations
   */
  public function translate($from, $to, $values, $object_type = '', $encodePlaceholders = FALSE, $entity = NULL) {
    $current_char_len    = 0;
    $request_value_lists = [];
    $current_list        = [];
    $lang_from           = substr($from, 0, 2);
    $lang_to             = substr($to, 0, 2);

    // Split into multiple arrays not exceeding max char limit.
    foreach ($values as $value) {
      $chars = strlen($value);
      if ($current_char_len + $chars > $this->maxCharsPerRequest) {
        $request_value_lists[] = $current_list;
        $current_list          = [$value];
        $current_char_len      = $chars;
      }
      else {
        $current_list[]    = $value;
        $current_char_len += $chars;
      }
    }
    if (!empty($current_list)) {
      $request_value_lists[] = $current_list;
    }

    // Send requests and merge results together.
    $full_result   = [];
    $request_count = count($request_value_lists);

    $percentage_completed = 0;
    $reference_id         = $this->updateProgressInfo(0, $object_type, $lang_from, $lang_to, $entity);

    for ($i = 0; $i < $request_count; $i++) {
      $this->checkClientDisconnected($reference_id, $entity);

      $non_empty_strings = array_filter(
        $request_value_lists[$i],
        function( $text ) {
          return !empty( $text );
        }
      );

      if (empty($non_empty_strings)) {
        $full_result = array_merge($full_result, $request_value_lists[$i]);
      }
      else {
        // Replace necessary special characters removed on HTML tree creation;
        // currently needed only for UI strings containing multiple values
        // in same string, separated by ETX char.
        $non_empty_strings = str_replace(array_keys($this->specialCharDict), $this->specialCharDict, $non_empty_strings);

        // Convert string[] to XML[].
        $xml_values = $this->translatableValuesToXml($non_empty_strings);

        $translatable_values = $xml_values;
        $encode_response     = NULL;
        if ($encodePlaceholders) {
          // Replace non-translatable string placeholders
          // with XML tags (for UI strings).
          $encode_response     = $this->encodePlaceholders($xml_values);
          $translatable_values = $encode_response->values;
        }

        // Translate XML[].
        $this->logger->debug("Sending @lang_from-@lang_to translation request @iteration/@total_iterations (@total_strings strings)",
          [
            '@lang_from' => $lang_from,
            '@lang_to' => $lang_to,
            '@iteration' => $i + 1,
            '@total_iterations' => $request_count,
            '@total_strings' => count($translatable_values),
          ]
        );
        $xml_translations = $this->sendTranslationRequest($lang_from, $lang_to, $translatable_values, $entity);

        if (!$xml_translations || empty($xml_translations)) {
          $retries_left = 2;
          while ($retries_left > 0 && (!$entity || $this->retryOnEntityTranslationError)) {
            $this->checkClientDisconnected($reference_id, $entity);
            $this->logger->debug("Translation request failed, retrying... (retries left: @retries_left)", ['@retries_left' => $retries_left]);
            $xml_translations = $this->sendTranslationRequest($lang_from, $lang_to, $translatable_values, $entity);
            $retries_left--;
          }

          // Do not continue if one of requests fail.
          if (!$xml_translations || empty($xml_translations)) {
            if ($request_count > 1) {
              $this->logger->error('One of translation requests failed. Please check if machine translation provider API is working or reduce the "Max characters per single request" value (in Translation provider configuration) and try again!');
            }
            return new TranslationResponse($full_result, empty($full_result) ? TranslationStatus::NO_RESULT : TranslationStatus::PARTIAL_RESULT);
          }
        }

        if ($encode_response) {
          // Restore non-translatable string placeholders.
          $xml_translations = $this->restoreSpacesAroundPlaceholders($translatable_values, $xml_translations, $encode_response->placeholders);
          $xml_translations = $this->decodePlaceholders($xml_translations, $encode_response->placeholders);
        }
        // Convert XML[] back to string[].
        $decoded_translations = $this->decodeXmlTranslations($non_empty_strings, $xml_translations);
        // Restore special characters (for some UI strings).
        $decoded_translations = str_replace($this->specialCharDict, array_keys($this->specialCharDict), $decoded_translations);
        // Merge with previous translations.
        $full_result = $this->mergeTranslationResults($request_value_lists[$i], $decoded_translations, $full_result);
      }
      // Update percentage for pre-translation progress bar.
      $percentage_completed += 100 / $request_count;
      $this->updateProgressInfo($percentage_completed, $object_type, $lang_from, $lang_to, $entity);
    }
    return new TranslationResponse($full_result, TranslationStatus::FULL_RESULT);
  }

  /**
   * Makes a language direction request to check if connection is successful.
   *
   * @return bool
   *   True, if MT API request is successful, false otherwise
   */
  public function testConnection() {
    $response = $this->sendLanguageDirectionRequest();
    if (!$response) {
      $this->logger->error('Response object is null. Please check if MT service can be reached.');
      return FALSE;
    }
    $content_type = isset($response->getHeaders()['Content-Type']) ? $response->getHeaders()['Content-Type'][0] : NULL;
    if ($content_type && !str_contains($content_type, 'application/json')) {
      $this->logger->error("Invalid content type '@content_type' in language direction response!", ['@content_type' => $content_type]);
      return FALSE;
    }
    return 200 === $response->getStatusCode();
  }

  /**
   * Maps language direction response to Generic API JSON response.
   *
   * @param string $body
   *   Response body.
   *
   * @return string
   *   JSON body
   */
  protected function mapLanguageDirectionResponseBody($body) {
    return (string) $body;
  }

  /**
   * Converts translatable strings to XML strings.
   *
   * @param string $values
   *   Translatable string values.
   *
   * @return string[]
   *   XML strings
   */
  protected function translatableValuesToXml($values) {
    $xmls  = [];
    $count = 1;
    foreach ($values as $v) {
      $html_string = $this->wrapAsHtml($v);
      $dom         = Html::load($html_string);

      $body  = $dom->getElementsByTagName('body')[0];
      $nodes = [$body->firstChild];

      while ($node = array_shift($nodes)) {
        if ($node instanceof \DOMElement) {
          // Replace each non-text node with <g> node
          // (or x tag if self-closing), also removing all attributes.
          $newTag = $dom->createElement($node->hasChildNodes() ? 'g' : 'x');
          // Set id for each <g> or <x> node.
          $newTag->setAttribute('id', $count++);
          // Replace node.
          $node->parentNode->replaceChild($newTag, $node);
          // Move all children nodes to new node.
          while ($node->hasChildNodes()) {
            $newTag->appendChild($node->firstChild);
          }
          // Process children nodes next.
          $nodes = array_merge(iterator_to_array($newTag->childNodes), $nodes);

          foreach ($this->translatableHtmlAttributes as $attr) {
            if ($node->hasAttribute($attr)) {
              // MT engines may not translate XML attributes, so create
              // temporary text nodes from attribute values.
              $a_tag = $dom->createElement('a');
              $a_tag->setAttribute('id', "$attr-$count");
              $a_tag->setAttribute('name', $attr);
              $a_tag->nodeValue = $node->getAttribute($attr);
              $newTag->appendChild($a_tag);
            }
          }
        }
      }
      $xmls[] = $dom->saveXML($body->firstChild);
    }
    return $xmls;
  }

  /**
   * Converts translated XML strings back to normal strings.
   *
   * @param string[] $source_values
   *   Original (normal) strings that were translated.
   * @param string[] $translated_values
   *   XML translations.
   *
   * @return string[]
   *   Translated strings
   */
  public function decodeXmlTranslations($source_values, $translated_values) {
    $translation_count    = count($translated_values);
    $decoded_translations = [];
    for ($i = 0; $i < $translation_count; $i++) {
      // Retrieve source and translated values.
      $source      = $source_values[$i];
      $translation = $translated_values[$i];

      // Load source as html document.
      $html_source  = $this->wrapAsHtml($source);
      $html_dom     = Html::load($html_source);
      $html_wrapper = $html_dom->getElementsByTagName('body')[0]->firstChild;

      // Load translation as xml document.
      $xml         = simplexml_load_string($translation);
      $xml_dom     = dom_import_simplexml($xml)->ownerDocument;
      $xml_wrapper = $xml_dom->firstChild;

      // Move translated attribute values from nodes back to attributes.
      $this->processAttributeNodes($xml_dom);
      // Replace text nodes in source html with
      // matching text nodes from translation xml.
      $new_node = $this->replaceTextNodesBottomUp($html_wrapper, $xml_wrapper);
      // Save as XML instead of HTML to avoid url encoding.
      $text_xml = $html_dom->saveXML($new_node);
      // Remove XML tag.
      $text_value = preg_replace('/\<\?xml(.*?)\?\>/', '', $text_xml);

      // Remove wrapper.
      $html_prefix_len        = strlen($this->htmlWrapperPrefix);
      $decoded_translation    = substr($text_value, $html_prefix_len, strlen($text_value) - $html_prefix_len - strlen($this->htmlWrapperSuffix));
      $decoded_translations[] = $decoded_translation;
    }
    return $decoded_translations;
  }

  /**
   * Remove XML attribute nodes and restore their values as parent attributes.
   *
   * @param \DOMDocument $dom
   *   Document.
   */
  private function processAttributeNodes($dom) {
    // Set parent node attributes from <a> tags
    // containing attribute translations.
    $attribute_nodes = $dom->getElementsByTagName('a');
    $count           = count($attribute_nodes);
    for ($i = 0; $i < $count; $i++) {
      // $attribute_nodes list is updated on removeChild, next
      // element's index is always 0.
      $node      = $attribute_nodes[0];
      $parent    = $node->parentNode;
      $attr_name = $node->getAttribute('name');
      $attr_val  = $node->textContent;
      $parent->setAttribute($attr_name, $attr_val);
      $parent->removeChild($node);
    }
  }

  /**
   * Replace strings with their translations.
   *
   * Traverse original and translated XML strings bottom up & replace
   * original string values with their translations.
   *
   * @param DOMNode $original_node
   *   Original XML node.
   * @param DOMNode $encoded_node
   *   Translated XML node.
   *
   * @return DOMNode
   *   Original node with text translated
   */
  private function replaceTextNodesBottomUp($original_node, $encoded_node) {
    // Recursively call this function on each child node.
    $original_child_node = $original_node->lastChild;
    $encoded_child_node  = $encoded_node->lastChild;
    while ($original_child_node && $encoded_child_node) {
      // Skip any extra text nodes in original HTML
      // (fix for Drupal 8 which inserts newlines between paragraph nodes).
      while ($original_child_node && $encoded_child_node && XML_TEXT_NODE === $original_child_node->nodeType && XML_TEXT_NODE !== $encoded_child_node->nodeType) {
        $original_child_node = $original_child_node->previousSibling;
      }

      $this->replaceTextNodesBottomUp($original_child_node, $encoded_child_node);
      $original_child_node = $original_child_node->previousSibling;
      $encoded_child_node  = $encoded_child_node->previousSibling;

      // Workaround for extra spaces inserted by MT
      // between <g> tags like: '</g> <g>'.
      while ($original_child_node && $encoded_child_node && XML_TEXT_NODE === $encoded_child_node->nodeType && empty(trim($encoded_child_node->nodeValue)) && XML_TEXT_NODE !== $original_child_node->nodeType) {
        $textNode = $original_node->ownerDocument->createTextNode($encoded_child_node->nodeValue);
        $original_node->insertBefore($textNode, $original_child_node->nextSibling);
        $encoded_child_node = $encoded_child_node->previousSibling;
      }
    }

    // Process the current node.
    if (XML_TEXT_NODE === $original_node->nodeType) {
      $original_node->nodeValue = $encoded_node->nodeValue;
    }
    elseif ($encoded_node instanceof \DOMElement) {
      // Copy translated attributes.
      foreach ($this->translatableHtmlAttributes as $attr) {
        if ($encoded_node->hasAttribute($attr)) {
          $original_node->setAttribute($attr, $encoded_node->getAttribute($attr));
        }
      }
    }
    return $original_node;
  }

  /**
   * Match Drupal variables with regex and encode them as XML tags.
   *
   * @param string[] $values
   *   XML translatable values.
   *
   * @return \stdClass
   *   Placeholder dictionary
   */
  private function encodePlaceholders($values) {
    $regex                  = '/' . implode('|', $this->drupalVariablePatterns) . '/';
    $response               = new \stdClass();
    $response->placeholders = [];
    $response->values       = [];
    $index                  = 1;
    foreach ($values as $v) {
      $response->values[] = preg_replace_callback(
      $regex,
      function ($matches) use (&$index, $response) {
        $key                          = $this->getPlaceholder($index++);
        $response->placeholders[$key] = $matches[0];
        return $key;
      },
                $v
      );
    }
    return $response;
  }

  /**
   * Replaces encoded placeholders with original values.
   *
   * @param string[] $translated_values
   *   Translated XML strings.
   * @param string[] $placeholders
   *   Placeholder dictionary.
   *
   * @return string[]
   *   Translations with non-translatable strings restored
   */
  private function decodePlaceholders($translated_values, $placeholders) {
    $translated_values = str_replace(array_keys($placeholders), $placeholders, $translated_values);
    return $translated_values;
  }

  /**
   * Restores spaces around encoded placeholders removed by MT service.
   *
   * @param string[] $original_xml_values
   *   Original XML values.
   * @param string[] $translated_xml_values
   *   Translated XML values.
   * @param string[] $placeholders
   *   Placeholder dictionary.
   *
   * @return string[]
   *   Translations
   */
  private function restoreSpacesAroundPlaceholders($original_xml_values, $translated_xml_values, $placeholders) {
    $delimiter      = '__WEBT__;';
    $original_xml   = implode($delimiter, $original_xml_values);
    $translated_xml = implode($delimiter, $translated_xml_values);

    foreach (array_keys($placeholders) as $key) {
      $original_pos    = strpos($original_xml, $key);
      $translation_pos = strpos($translated_xml, $key);
      if ($original_pos > -1 && $translation_pos > -1) {
        $original_char_before    = substr($original_xml, $original_pos - 1, 1);
        $translation_char_before = substr($translated_xml, $translation_pos - 1, 1);
        if (ctype_space($original_char_before) && !ctype_space($translation_char_before)) {
          $translated_xml = str_replace($key, $original_char_before . $key, $translated_xml);
        }
        $original_char_after    = substr($original_xml, $original_pos + strlen($key), 1);
        $translation_char_after = substr($translated_xml, $translation_pos + strlen($key), 1);
        if (ctype_space($original_char_after) && !ctype_space($translation_char_after)) {
          $translated_xml = str_replace($key, $key . $original_char_after, $translated_xml);
        }
      }
    }
    return explode($delimiter, $translated_xml);
  }

  /**
   * Generates an XML tag to encode a Drupal variable (placeholder).
   *
   * @param int $index
   *   Index of the placeholder.
   *
   * @return string
   *   XML tag
   */
  protected function getPlaceholder($index) {
    return "<x id=\"#$index\"/>";
  }

  /**
   * Wrap translatable string in HTML tags.
   *
   * @param string $string
   *   Source string.
   *
   * @return string
   *   HTML node string
   */
  private function wrapAsHtml($string) {
    return $this->htmlWrapperPrefix . $string . $this->htmlWrapperSuffix;
  }

  /**
   * Update progress bar info and return pre-translation request ID.
   *
   * @param float $percentage
   *   Percentage completed.
   * @param string $type
   *   String content type.
   * @param string $from
   *   Source language.
   * @param string $to
   *   Target language.
   * @param Drupal\Core\Entity\EntityInterface $entity
   *   Related entity.
   *
   * @return string|null
   *   Pre-translation request ID or null
   */
  private function updateProgressInfo($percentage, $type, $from, $to, $entity) {
    $pretranslationProgressController = \Drupal::service('webt.pretranslation_progress_controller');
    if (!$entity && $pretranslationProgressController->getValue()) {
      $pretranslationProgressController->updateTranslationMessage($this->t("Translating strings of type '@type' (@from -> @to)",
        [
          '@type' => $type,
          '@from' => $from,
          '@to' => $to,
        ]
      ));
      $pretranslationProgressController->updateTranslationPercentage($percentage);
      return $pretranslationProgressController->getValue()->data->request_id;
    }
    return NULL;
  }

  /**
   * Check if client has disconnected and translation process has to be aborted.
   *
   * @param string $reference_id
   *   Pre-translation request ID.
   * @param Drupal\Core\Entity\EntityInterface $entity
   *   Related entity.
   */
  private function checkClientDisconnected($reference_id, $entity) {
    $pretranslationProgressController = \Drupal::service('webt.pretranslation_progress_controller');
    $value = $pretranslationProgressController->getValue();
    if (!$entity && $value && $value->data) {
      if ($reference_id && $reference_id !== $value->data->request_id) {
        $this->logger->debug('Stopping execution, parallel translation detected!');
        die();
      }
      if ($value->data->aborted) {
        $this->logger->debug('Stopping execution, client disconnected!');
        die();
      }
    }
  }

  /**
   * Adds translated string batch to full result list. If source string is empty, creates and adds empty translation string.
   *
   * @param string[] $source_strings List of source strings
   * @param string[] $translated_strings List of translations for non-empty source strings
   * @param string[] $all_translations All translations, including previously added
   * @return string[] Merged translatioins
   */
  private function mergeTranslationResults($source_strings, $translated_strings, $all_translations) {
    $translation_offset = 0;
    foreach ( $source_strings as $original ) {
      if ( empty( $original ) ) {
        $all_translations[] = '';
      } else {
        $all_translations[] = $translated_strings[ $translation_offset++ ];
      }
    }
    return $all_translations;
  }

}

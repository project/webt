<?php

namespace Drupal\webt\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides empty MT notice block.
 *
 * @Block(
 *   id = "webt_mt_notice",
 *   admin_label = @Translation("Machine translation notice"),
 * )
 */
class MtNoticeBlock extends BlockBase {

  /**
   * Build definition.
   */
  public function build() {
    $build = [];

    $build['#attached']['library'][] = 'webt/mt_notice';

    $mt_notice_settings           = new \stdClass();
    $mt_notice_settings->text     = $this->t('This page has been machine-translated.');
    $mt_notice_settings->linkText = $this->t('Show original');

    $build['#attached']['drupalSettings']['mt_notice_settings'] = $mt_notice_settings;

    return $build;
  }

}
